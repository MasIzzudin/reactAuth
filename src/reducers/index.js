import { combineReducers } from 'redux';
import authReducers from './authReducers';
import navReducers from './navReducers';

export default combineReducers({
    auth: authReducers,
    nav: navReducers
});
