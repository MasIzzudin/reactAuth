
const State_hajimari = {
    loggedIn: false
}

export default (state = State_hajimari, action) => {
    switch (action.type) {
        case 'Login':
            return { ...state, loggedIn: true, ...action.payload }
        case 'Logout':
            return { ...state, loggedIn: false }
        default:
            return state;
    }
}