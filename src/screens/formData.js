import React, { Component } from 'react';
import { 
    Form, Content, List, 
    ListItem, Left, Right, Body, Text
} from 'native-base';
import { AsyncStorage } from 'react-native';

export class FormData extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            password: ''
        }
        
        AsyncStorage.getItem('data', (error, result) => {
            if (result) {
                const resultParse = JSON.parse(result)
                console.log(resultParse.username)
                this.setState({
                    username: resultParse.username,
                    password: resultParse.password
                })
            } else {
                console.log('tidak ada data')
            }
        })
    }

    render() {
        return (
            <Form style={{ flex: 1 }}>
                <Content>
                    <List>
                        <ListItem>
                            <Left>
                                <Text>{this.state.username}</Text>
                            </Left>

                            <Right>
                                <Text>{this.state.password}</Text>
                            </Right>
                        </ListItem>
                    </List>
                </Content>
            </Form>   
        )
    }
}