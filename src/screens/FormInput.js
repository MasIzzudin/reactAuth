import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AsyncStorage } from 'react-native';
import { Form, Button, Input, Item, Text, ListItem, Body, CheckBox } from 'native-base';
import { onRegister, authentication, onLogin } from '../action/authAction'

class FormInput extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            password: ''
        }
    };

    async componentWillMount() {
        try {
            const access_token = await AsyncStorage.getItem('access')
            console.log(access_token)
            this.props.authentication(access_token)
        } catch (error) {
            console.log(error)
        }
    }

    registerResponse() {
        const { username, password } = this.state;
        if (username != '' && password != '') {
            this.props.onRegister({ username, password })
        } else {
            alert('username and password is required')
        }

        const dataUser = {
            username: username,
            password: password
        }

        AsyncStorage.setItem('data', JSON.stringify(dataUser))
    }

    loginResponse() {
        const { username, password } = this.state;
        if (username != '' && password != '') {
           this.props.onLogin({ username, password })
        } else {
            alert('username and password is required')
        }
    }

    render() {
        return (
            <Form style={styles.container}>
                <Item regular>
                    <Input 
                        style={styles.textInput} placeholder='email@email.com' 
                        onChangeText={text => this.setState({ username: text })}
                    />
                </Item>

                <Item regular>
                    <Input 
                        style={styles.textInput} 
                        placeholder='Password'
                        secureTextEntry
                        onChangeText={text => this.setState({ password: text })} 
                    />
                </Item>
                
                <Button block onPress={this.registerResponse.bind(this)}>
                    <Text>Daftar</Text>
                </Button>

                <Button block  success style={{ marginTop: 10 }} onPress={this.loginResponse.bind(this)}>
                    <Text>Login</Text>
                </Button>

                <Form style={styles.FooterStyle}>
                    <Text style={styles.textStyle}> Lupa Password? 
                    <Text 
                    style={[styles.textStyle, { color: 'red' }]}
                    onPress={() => this.props.navigation.navigate('FormData')}
                    > 
                        Click Here </Text> 
                    </Text>
                </Form>
            </Form>
        )
    }
}

export default connect(null, { onRegister, authentication, onLogin })(FormInput);

const styles = {
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        padding: 16,
        paddingTop: 32
      },
      textInput: {
        height: 35,
        backgroundColor: 'white',
        marginTop: 8,
        marginBottom: 8,
        borderWidth: 1,
        borderColor: 'grey',
        padding: 8
      },

      FooterStyle: {
          justifyContent: 'flex-end',
          alignItems: 'flex-end'
      },

      textStyle: {
          fontSize: 15
      }
}
