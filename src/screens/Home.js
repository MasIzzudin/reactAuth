import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Form, Text, Button } from 'native-base';
import { onLogOut } from '../action/authAction';

class Home extends Component {

    render() {
        console.log(this.props)
        const { access_token } = this.props.auth;

        return (
            <Form style={Style.container}>
                <Text>{access_token}</Text>
                
                <Button danger block onPress={() => this.props.onLogOut()}>
                    <Text>Keluar</Text>
                </Button>
            </Form>
        );
    }
}

const Style = {
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps, { onLogOut })(Home);