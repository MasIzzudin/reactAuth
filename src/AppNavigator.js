import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StackNavigator, addNavigationHelpers } from 'react-navigation';
import FormInput from './screens/FormInput';
import Home from './screens/Home';
import { FormData } from './screens/formData';

export const Root = StackNavigator({
    FormInput: {
        screen: FormInput,
        navigationOptions: { header: null }
    },
    Home: {
        screen: Home,
        navigationOptions: { 
            title: 'Home',
            headerStyle: {
                backgroundColor: 'white',
                elevation: 1,
                paddingTop: 5
            }, 
        }, 
    },
    FormData: {
        screen: FormData,
        navigationOptions: { 
            title: 'Data Kamu :*'
        }
    }
})

const AppNavigator = ({ dispatch, nav }) => {
    return (
        <Root navigation={addNavigationHelpers({ dispatch, state: nav })} />
    );
}

AppNavigator.propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired
  };

const mapStateToProps = state => ({
    nav: state.nav
})

export default connect(mapStateToProps)(AppNavigator);