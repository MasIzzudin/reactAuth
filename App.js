import React, { Component } from 'react';
import ReduxThunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { View } from 'react-native';
import AppNavigator from './src/AppNavigator';

import reducers from './src/reducers';

const loggerMiddleware = createLogger();

export class App extends Component {
  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

      return (
          <Provider store={store}>
            <AppNavigator />
          </Provider>
      )
  }
}
